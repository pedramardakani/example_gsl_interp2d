/*********************************************************************
GSL 2D spline example.

This program is a temporary example to help us develop a 2D interpolation
library inside Gnuastro and fulfill the following task:
https://savannah.gnu.org/task/?14419

Original author:
     Nimish Purohit <nimishpurohit2@gmail.com>
Contributing author(s):
     Pedram Ashofteh Ardakani <pedramardakani@pm.me>
Copyright (C) 2022 Free Software Foundation, Inc.

Gnuastro is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

Gnuastro is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with Gnuastro. If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

#include <errno.h>
#include <error.h>
#include <stdio.h>
#include <stdlib.h>

#include <gsl/gsl_spline2d.h>
#include <gsl/gsl_interp2d.h>

#include <gnuastro/fits.h>
#include <gnuastro/blank.h>

enum{
GAL_INTERPOLATE_2D_BICUBIC,
GAL_INTERPOLATE_2D_BILINEAR,
};

gsl_spline2d *
gal_interpolate_2d_make_gsl_spline2d(gal_data_t *X, gal_data_t *Y,
                                     gal_data_t *Z, int type_2d)
{
  //size_t i;
  double *x, *y, *z;
  gal_data_t *Xd, *Yd, *Zd;
  gsl_spline2d *spline=NULL;
  const gsl_interp2d_type *itype=NULL;
  int Zhasblank=gal_blank_present(Z, 0);

  /* A small sanity check.
     if(Z->ndim != 1)
     error(EXIT_FAILURE, 0, "%s: input dataset is not 1D (it is %zuD)",
     __func__, Z->ndim); */

  if( X && gal_blank_present(X, 0) )
    error(EXIT_FAILURE, 0, "%s: the X dataset has blank elements",
          __func__);

  if( Y && gal_blank_present(Y, 0) )
    error(EXIT_FAILURE, 0, "%s: the Y dataset has blank elements",
          __func__);

  if( X && Y && X->dsize[0]*Y->dsize[0]!=Z->dsize[0]*Z->dsize[1] )
    error(EXIT_FAILURE, 0, "%s: error!", __func__);

  if( !X && !Y )
    error(EXIT_FAILURE, 0, "%s: at least X or Y should be there!",
          __func__);

  /* Set the interpolation type. */
  switch(type_2d)
    {
    case GAL_INTERPOLATE_2D_BICUBIC:
      itype=gsl_interp2d_bicubic;  break;
    case GAL_INTERPOLATE_2D_BILINEAR:
      itype=gsl_interp2d_bilinear; break;
    default:
      error(EXIT_FAILURE, 0, "%s: code %d not recognizable for the GSL "
            "interpolation type", __func__, type_2d);
    }

  /* Initializations. Note that if Z doesn't have any blank elements and is
     already in `double' type, then we don't need to make a copy. */
  Zd=( (Zhasblank || Z->type != GAL_TYPE_FLOAT64)
       ? gal_data_copy_to_new_type(Z, GAL_TYPE_FLOAT64)
       : Z );

  /* Fill in the X axis values while also removing NaN/blank elements. */
  if(X)
    {
      if( X->type != GAL_TYPE_FLOAT64 || Zhasblank )
        Xd = gal_data_copy_to_new_type(X, GAL_TYPE_FLOAT64);
      else
        Xd = X;
    }

  if(Y)
    {
      if( Y->type != GAL_TYPE_FLOAT64 || Zhasblank )
        Yd=gal_data_copy_to_new_type(Y, GAL_TYPE_FLOAT64);
      else
        Yd=Y;
    }

  /*
  if(X && !Y)
    {
      if( Z->dsize[0] % X->dsize[0] == 0 && Z->dsize[0] >= X->dsize[0] )
        Yd=gal_data_alloc(NULL, GAL_TYPE_FLOAT64, 1,
                          Z->dsize[0]/X->dsize[0], NULL, 0, -1, NULL,
                          NULL, NULL);
      else
        error(EXIT_FAILURE, 0, "%s: cannot do!", __func__);
    }
  else if( !X && Y )
    {
      if( Z->dsize % Y->dsize == 0 && Z->dsize >= Y->dsize )
        Xd=gal_data_alloc(NULL, GAL_TYPE_FLOAT64, 1,
                          int(Z->dsize/Y->dsize), NULL, 0, -1, NULL,
                          NULL, NULL);
      else
        error(EXIT_FAILURE, 0, "%s: cannot do!", __func__);
    }
  */

  //size_t c=0;
  x=Xd->array; y=Yd->array; z=Zd->array;
  if(!X) for(int i=0; i < Z->dsize[0]/Y->dsize[0]; ++i) { x[i]=i; }
  if(!Y) for(int i=0; i < Z->dsize[0]/X->dsize[0]; ++i) { y[i]=i; }

  /* Make sure we have enough valid points for interpolation. */
  if( Z->dsize[0] >= gsl_interp2d_type_min_size(itype) )
    {
      spline=gsl_spline2d_alloc(itype, Xd->dsize[0], Yd->dsize[0]);
      gsl_spline2d_init(spline, x, y, z, Xd->dsize[0], Yd->dsize[0]);
    }
  else
    spline=NULL;

  /* Clean up and return. */
  if(Xd != X) gal_data_free(Xd);
  if(Yd != Y) gal_data_free(Yd);
  if(Zd != Z) gal_data_free(Zd);

  return spline;
}


int
main()
{
  float *farray;
  gsl_spline2d *spl;
  gal_data_t *image, *Xa, *Ya;
  size_t i, syz_a[1], syz_b[1];
  double sum=0.0f, j, k, z, *xa, *ya;
  char *filename="2_wcscat.fits", *hdu="1";
  gsl_interp_accel *xacc=gsl_interp_accel_alloc();
  gsl_interp_accel *yacc=gsl_interp_accel_alloc();

  /* Read `img.fits' (HDU: 1) as a float32 array. */
  image=gal_fits_img_read_to_type(filename, hdu, GAL_TYPE_FLOAT32, -1, 1);
  farray=image->array;

  /* Use the allocated space as a single precision floating point array
   * (recall that `image->array' has `void *' type, so it is not directly
   * usable. */

  xa=(double*) malloc(image->dsize[0]*sizeof(double));
  ya=(double*) malloc(image->dsize[1]*sizeof(double));

  for(i=0; i<image->dsize[0]; i+=1) { xa[i]=i; }
  syz_a[0]=i;

  for(i=0; i<image->dsize[1]; i+=1) { ya[i]=i; }
  syz_b[0]=i;

  Xa=gal_data_alloc(xa, 11, 1, syz_a, NULL, 0, 1000000, 0, NULL, NULL,
                     NULL);
  Ya=gal_data_alloc(ya, 11, 1, syz_b, NULL, 0, 1000000, 0, NULL, NULL,
                     NULL);

  spl=gal_interpolate_2d_make_gsl_spline2d(Xa, Ya, image, 0);

  for(j=400.2; j<401.0; j+=0.2)
    for(k=411.2; k<412.0; k+=0.2)
      {
        z=gsl_spline2d_eval(spl, j, k, xacc, yacc);
        printf(" %f ",z);
      }

  for(i=0; i<image->size; ++i) { sum+=farray[i]; }

  return EXIT_SUCCESS;
}
